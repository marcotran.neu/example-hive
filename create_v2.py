from pyspark.sql import SparkSession
from pyspark.sql.types import *
spark = SparkSession.builder \
  .appName("Create Hive ACID Table from DataFrame") \
  .getOrCreate()

data = [("John Doe", 30), ("Jane Doe", 25)]
columns = ["name", "age"]
df = spark.createDataFrame(data, columns)

schema = StructType([
  StructField("name", StringType(), True),
  StructField("age", IntegerType(), True)
])

spark.sql(
    f"CREATE TABLE IF NOT EXISTS hive_acid_orc_table USING ORC TBLPROPERTIES ('transactional'='true')"
)
spark.sql(
    f"CREATE TABLE hive_acid_table USING ORC TBLPROPERTIES ('transactional'='true')"
)

df.write.saveAsTable("hive_acid_orc_table")
df.write.saveAsTable("hive_acid_table", schema=schema)