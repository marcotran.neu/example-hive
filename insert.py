from pyspark.sql import SparkSession

# Create a Spark session
spark = SparkSession.builder.appName("example").enableHiveSupport().getOrCreate()

# Specify the database and table name
database_name = "extract_test"
table_name = "tbl_acid"

# Sample new data to insert
new_data = [("Janet", 22, "CL")]

# Create a DataFrame for the new data
columns = ["name", "age", "port_code"]
new_df = spark.createDataFrame(new_data, columns)

# Use the DataFrame API to insert the new data into the Hive table
spark.sql(f"USE {database_name}")
new_df.write.format("orc").mode("append").insertInto(table_name)