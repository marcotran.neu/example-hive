from pyspark.sql import SparkSession
from pyspark.sql.functions import coalesce

# Create a Spark session
spark = SparkSession.builder.appName("example").enableHiveSupport().getOrCreate()

# Specify the database and table name
database_name = "extract_test"
table_name = "tbl_acid"

# Sample updated data
updated_data = [("John", 35, "CL")]

# Create a DataFrame for the updated data
columns = ["name", "age", "port_code"]
updated_df = spark.createDataFrame(updated_data, columns)
updated_df.cache()

# Read existing data from Hive
existing_df = spark.table(f"{database_name}.{table_name}")
existing_df.cache()

# Specify the columns to be used for the join
join_columns = ["name", "port_code"]

# Specify the columns to be updated
columns_to_update = ["age"]

# Create a list of columns for the select statement
select_columns = [existing_df[col] for col in existing_df.columns]

# Update the existing DataFrame with the new data
for col in columns_to_update:
    select_columns.append(coalesce(updated_df[col], existing_df[col]).alias(col))

# Perform the join with multiple keys
join_condition = [existing_df[col] == updated_df[col] for col in join_columns]
merged_df = existing_df.join(updated_df, join_condition, "outer").select(*select_columns)

# Save the merged DataFrame back to Hive, overwriting the existing data
merged_df.write.format("orc").mode("overwrite").insertInto(f"{database_name}.{table_name}")