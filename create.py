from pyspark.sql import SparkSession

# Create a Spark session
spark = SparkSession.builder.appName("example").enableHiveSupport().getOrCreate()

# Specify the database and table name
database_name = "extract_test"
table_name = "tbl_acid"

# Sample DataFrame for schema inference (replace with your actual data)
data = [("John", 25, "CL"), ("Alice", 30, "CL"), ("Bob", 28, "HP")]
columns = ["name", "age", "port_code"]
df = spark.createDataFrame(data, columns)

# Define the Hive table schema and partition columns
schema = df.schema
partition_columns = ["port_code"]  # Add partition columns if needed

# Create the Hive table with ACID properties
spark.sql(f"CREATE DATABASE IF NOT EXISTS {database_name}")
spark.sql(f"USE {database_name}")
spark.sql(f"""
    CREATE TABLE IF NOT EXISTS {table_name}
    USING ORC
    CLUSTERED BY (name) INTO 16 BUCKETS
    {f'PARTITIONED BY ({", ".join(partition_columns)})' if partition_columns else ''}
    STORED AS ORC
    TBLPROPERTIES (
        'transactional'='true',
        'transactional_properties'='insert_only'
    )
""")
